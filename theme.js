import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  colors: {
    darkgray: "hsl(0, 0%, 63%)",
    black: "hsl(0, 0%, 0%)",
    white: "hsl(0, 0%, 100%)",
    verydarkgray: "hsl(0, 0%, 27%)",
  },
  fonts: {
    body: "Spartan, system-ui, sans-serif",
  },
});

export default theme;
