import {
  Box,
  Heading,
  Image,
  Text,
  useBreakpointValue,
  Link,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import NextLink from "next/link";
import { useState } from "react";
import Head from "../components/Head";
import Navbar from "../components/Navbar";

export default function Home({ furniture }) {
  const [currDataIndex, setCurrDataIndex] = useState(0);
  const [isDrawerOpen, setDrawerState] = useState(false);

  const heroImageSource = useBreakpointValue({
    base: `mobile-image-${furniture[currDataIndex].image}`,
    md: `desktop-image-${furniture[currDataIndex].image}`,
  });

  const nextImage = () => {
    if (currDataIndex === furniture.length - 1) {
      setCurrDataIndex(0);
      return;
    }
    setCurrDataIndex(currDataIndex + 1);
  };

  const prevImage = () => {
    if (currDataIndex === 0) {
      setCurrDataIndex(furniture.length - 1);
      return;
    }
    setCurrDataIndex(currDataIndex - 1);
  };

  return (
    <div>
      <Navbar {...{ isDrawerOpen, setDrawerState }} />
      <Head title="Room | Home" />
      <Box
        maxHeight={{ base: isDrawerOpen ? "100vh" : "", xl: "100vh" }}
        overflow={{ base: isDrawerOpen ? "hidden" : "", xl: "hidden" }}
      >
        <Box
          display={{ xl: "grid" }}
          gridTemplateColumns={{ xl: "1fr 10% 1fr" }}
        >
          <Box position="relative" gridColumnEnd={{ xl: "span 2" }}>
            <Image
              src={`/images/${heroImageSource}`}
              alt="hero-images"
              width={{
                base: "100%",
              }}
              height={{
                // xl: "70vh",
                xl: "70vh",
                "2xl": "65vh",
              }}
            />
            <Box
              position="absolute"
              bottom={0}
              right={{ base: 0, xl: -(75 * 2) }}
              display="flex"
              alignItems="center"
              justifyContent="center"
            >
              <Box
                role="button"
                onClick={prevImage}
                onKeyPress={(e) => {
                  if (e.code === "Enter") prevImage();
                }}
                tabIndex="0"
                bg="black"
                color="white"
                boxSize="sm"
                width={{ base: "50px", md: "75px" }}
                height={{ base: "50px", md: "75px" }}
                display="flex"
                alignItems="center"
                justifyContent="center"
                cursor="pointer"
                _hover={{ bg: "verydarkgray" }}
                _focus={{ bg: "verydarkgray" }}
              >
                <Image
                  src="/images/icon-angle-left.svg"
                  alt="previous-item-button"
                  width="10px"
                />
              </Box>
              <Box
                role="button"
                onClick={nextImage}
                onKeyPress={(e) => {
                  if (e.code === "Enter") nextImage();
                }}
                tabIndex="0"
                bg="black"
                color="white"
                boxSize="xl"
                width={{ base: "50px", md: "75px" }}
                height={{ base: "50px", md: "75px" }}
                display="flex"
                alignItems="center"
                justifyContent="center"
                cursor="pointer"
                _hover={{ bg: "verydarkgray" }}
                _focus={{ bg: "verydarkgray" }}
              >
                <Image
                  src="/images/icon-angle-right.svg"
                  alt="next-item-button"
                  width="10px"
                />
              </Box>
            </Box>
          </Box>
          <Box alignSelf={{ xl: "center" }} my={{ base: "4rem", xl: 0 }}>
            <Box px={{ base: "2rem", md: "6rem", xl: "3rem", "2xl": "7rem" }}>
              <Heading
                as="h1"
                size="2xl"
                fontSize={{ base: "3xl", lg: "4xl", xl: "5xl", "2xl": "6xl" }}
                pb={{ base: "1.5rem", lg: "1.5rem", "2xl": "2rem" }}
              >
                {furniture[currDataIndex].title}
              </Heading>
              <Text
                pb={{ base: "3.5rem", lg: "1.5rem", "2xl": "2rem" }}
                color="darkgray"
                // fontSize={{ base: "sm", xl: "sm" }}
              >
                {furniture[currDataIndex].desc}
              </Text>
              <Box>
                <Link as={NextLink} href="/">
                  <Wrap
                    spacing="2rem"
                    align="center"
                    cursor="pointer"
                    _hover={{ opacity: 0.5 }}
                    _focus={{ opacity: 0.5 }}
                  >
                    <WrapItem
                      textTransform="uppercase"
                      letterSpacing="1rem"
                      tabIndex={1}
                    >
                      Shop now
                    </WrapItem>
                    <WrapItem>
                      <Image src="/images/icon-arrow.svg" />
                    </WrapItem>
                  </Wrap>
                </Link>
              </Box>
            </Box>
          </Box>
        </Box>
        <Box
          height={{ xl: "35vh" }}
          display={{ xl: "grid" }}
          gridTemplateColumns={{
            xl: "1fr 1fr 0.5fr 1fr",
            "2xl": "1fr 8.5% 1fr 10% 1fr",
          }}
        >
          <Box>
            <Image
              src={`/images/image-about-dark.jpg`}
              alt="dark-image"
              height={{ xl: "30vh", "2xl": "35vh" }}
              objectFit="cover"
              width={{
                base: "100%",
              }}
            />
          </Box>
          <Box
            alignSelf={{ xl: "center" }}
            gridColumnEnd={{ xl: "span 2", "2xl": "span 2" }}
            my={{ base: "4rem", xl: 0 }}
            // px={{ base: "2rem",  }}
            px={{ base: "2rem", md: "6rem", xl: "3rem", lg: "4rem" }}
          >
            <Heading
              as="h5"
              size="md"
              fontSize={{ base: "xl", xl: "xl" }}
              pb="1rem"
              textTransform="uppercase"
              letterSpacing="0.5rem"
            >
              About our furniture
            </Heading>
            <Text
              pb="1rem"
              color="darkgray"
              fontSize={{ base: "sm", "2xl": "md" }}
            >
              Our multifunctional collection blends design and function to suit
              your individual taste. Make each room unique, or pick a cohesive
              theme that best express your interests and what inspires you. Find
              the furniture pieces you need, from traditional to contemporary
              styles or anything in between. Product specialists are available
              to help you create your dream space.
            </Text>
          </Box>
          <Box gridColumnEnd={{ xl: "span 1", "2xl": "span 2" }}>
            <Image
              src={`/images/image-about-light.jpg`}
              alt="dark-image"
              height={{ xl: "30vh", "2xl": "35vh" }}
              objectFit="cover"
              width={{
                base: "100%",
              }}
            />
          </Box>
        </Box>
      </Box>
      <Box
        fontSize="x-small"
        position="absolute"
        bottom={0}
        left="50%"
        transform="translateX(-50%)"
      >
        Challenge by{" "}
        <Link
          as={NextLink}
          // passHref
          href="//www.frontendmentor.io?ref=challenge"
          target="_blank"
        >
          Frontend Mentor
        </Link>
        . Coded by{" "}
        <Link
          as={NextLink}
          // passHref
          href="//github.com/oreoluwa-bs"
          target="_blank"
        >
          Oreoluwa Bimbo-Salami
        </Link>
        .
      </Box>
    </div>
  );
}

Home.getInitialProps = async (ctx) => {
  const { req } = ctx;

  let host = req?.headers?.host || "localhost:3000";

  const response = await fetch(`http://${host}/api/furniture`);
  const parsedJSON = await response.json();
  return { furniture: parsedJSON };
};
