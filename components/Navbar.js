import { Box, Image, Link as ChakraLink } from "@chakra-ui/react";
import Link from "next/link";
// import { useState } from "react";

const links = [
  { href: "/", label: "home" },
  { href: "/", label: "shop" },
  { href: "/", label: "about" },
  { href: "/", label: "contact" },
].map((link) => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

const NavLinks = () => {
  return (
    <nav>
      <Box
        as="ul"
        display="inline"
        margin={{ md: "0 5rem" }}
        color={{ base: "black", md: "white" }}
      >
        {links.map(({ key, href, label }) => (
          <Box
            as="li"
            key={key}
            display="inline-block"
            margin="0 1rem"
            fontSize={{ base: "sm", md: "md" }}
            _hover={{
              position: "relative",
              _after: {
                bg: "white",
                width: "40%",
                height: "2px",
                opacity: 1,
              },
            }}
            _after={{
              display: "block",
              content: `""`,
              position: "absolute",
              left: "50%",
              // mx: "auto",
              transition: "all 1s",
              transform: "translateX(-50%)",
              opacity: 0,
            }}
          >
            <ChakraLink href={href} as={Link}>
              {label}
            </ChakraLink>
          </Box>
        ))}
      </Box>
    </nav>
  );
};

const Navbar = ({ isDrawerOpen, setDrawerState }) => {
  return (
    <Box
      as="header"
      maxWidth={{ xl: "1400px" }}
      px={{ base: "4rem", lg: "5rem" }}
      mx="auto"
      position="relative"
      bg="blue.500"
    >
      <Box
        // bg="orange.500"
        px={{ base: "1rem", md: "6rem", xl: "4rem", "2xl": 0 }}
        width="100%"
        display="flex"
        alignItems="center"
        justifyContent={{ base: "space-between", md: "start" }}
        position="absolute"
        top={{ base: "60px" }}
        left={0}
        zIndex={9}
      >
        <Box
          display={{ md: "none" }}
          cursor="pointer"
          onClick={() => setDrawerState(true)}
          tabIndex="0"
          role="button"
        >
          <Image
            src="/images/icon-hamburger.svg"
            height="18px"
            alt="menu button"
          />
        </Box>
        <ChakraLink as={Link} href="/">
          <Image src="/images/logo.svg" height="18px" alt="room logo" />
        </ChakraLink>

        <Box display={{ md: "none" }} height="18px" width="16px" />

        <Box display={{ base: "none", md: "block" }}>
          <NavLinks />
        </Box>

        <Box
          display={{ base: "block", md: "none" }}
          position="absolute"
          bg="blackAlpha.600"
          top={"-60px"}
          left={0}
          minHeight="100vh"
          width="100vw"
          transition="all 1s"
          transform={isDrawerOpen ? "translateY(0)" : "translateY(-100%)"}
        >
          <Box
            transition="all 1s"
            transform={isDrawerOpen ? "translateX(0)" : "translateX(-100%)"}
            position="absolute"
            bg="white"
            height="140px"
            width="100%"
            display="flex"
            alignItems="center"
            justifyContent="space-around"
          >
            <>
              <Box
                display={{ md: "none" }}
                cursor="pointer"
                onClick={() => setDrawerState(false)}
                onKeyPress={(e) => {
                  if (e.code === "Enter") setDrawerState(false);
                }}
                role="button"
                tabIndex="0"
              >
                <Image
                  src="/images/icon-close.svg"
                  height="16px"
                  alt="close menu button"
                />
              </Box>
              <NavLinks />
            </>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Navbar;
